<?php

/**
 * @file
 * The submission emailing system to inform the appropriate recipients
 * 
 */

/**
 * Function to call Drupal's email function when a TPR is submitted.
 * 
 *  @param array $TPS
 *    The reference variable post processing
 *  @param string $type
 *    Type of TPS action to alert
 *  @param $to
 *    Chance to add additional emial addresses to the mailing
 * 
 *  @return
 *    TODO: Return the status of the emails sent
 */
 
 function emugetpr_submit_emails(&$TPS, $type = 'submission', $to = array('developer@emuge.com')) {
  
  $params = array(
    'TPS' => $TPS,
  );
  
  $message = emuge_tpr_prepare_mail($type, $params);
   
    if(is_array($to)) {
      foreach($to as $rec) {
     $results =   mail($rec, $message['subject'], $message['body'], $message['headers']);
      }
    } 
    else {
     $results =   mail($to, $message['subject'], $message['body'], $message['headers']);
    }
}

function emuge_tpr_prepare_mail($key, $params) {
  $message = array();
  $headers = 'From: Emuge TPR Submission <submissions@emuge.com>' .  "\r\n";
  $headers .= 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  /**/
  $headers .= 'Rick Phillips <rickwphillips@gmail.com>' . "\r\n";
  
  #$headers .= 'Cc: Rick Phillips <rick.phillips@emuge.com>';
  
  #$message['headers']['Bcc'] = $params['headers']['Bcc'];
  $message['headers'] = $headers;
  $TPS = $params['TPS'];
  unset($params['TPS']);
  
  switch ($key) {
    case 'submission' :
      // Set subject to to the newly submitted text
      $message['subject'] = $TPS['sales_manager'] . ' has submitted a Tool Performance Report. Results: ' . $TPS['success'];
      $type = 'New Submission';
      break;  
    
    case 'update' :
      // Set subject to to the newly updated text
      $message['subject'] = $TPS['sales_manager'] . ' has updated Tool Performance Report #' . $TPS['sid'];
      $type = 'Submission Update';
      break;
    
    default :
      // No default action
  }
    #$message['headers'] = $params['headers'];
    // Add the default body to the message
    $message['attachments'][] = file_load($TPS['attachments']['fid']);
    $message['body'] = "<h3>Emuge Corporation Tool Performance Report #{$TPS['sid']} - $type</h3>";
    $message['body'] .= 'The report can be seen <a href="http://www.emuge.com/ec-tpr/submission/' . $TPS['sid'] . '">here.</a><br/><br />';
    $message['body'] .= 'Make sure to log in if you are not granted access to the reports.<br/><br />';
    $message['body'] .= '<em>This is an automated message from submissions@emuge.com. <br />Please do not reply to this email.</em><br/><br /><br/>';
    $message['body'] .= '<img  src="http://www.emuge.co/sites/default/files/logo_small.png" /><br/>';
    
    return $message;
}

function __emugetpr_submit_emails(&$TPS, $type = 'submission', $to = array('developer@emuge.com')) {
  $params = array();
  $header = array(
    'Bcc' => "rickwphillips@gmail.com, rick.phillips@emuge.com, jamie.fiffy@emuge.com",
    'Content-Transfer-Encoding' => "8Bit",
    'Content-Type' => 'text/html; charset=UTF-8;',
    'MIME-Version' => "1.0",
    
  );
  // Two versions of the mail to list for testing
  $test_bcc = 'rickwphillips@gmail.com';
  $live_bcc = 'rickwphillips@gmail.com';
  
  // Set to live
  $params['headers'] = $header;
  $params['TPS'] = $TPS;
  
  // Send multiple emails if an array of names is passed in $to
  if (is_array($to)) {
    foreach($to as $recipient) {
      drupal_mail('emuge_tpr', $type, $recipient, user_preferred_language(1), $params, 'submissions@emuge.com', false);
      mail($recipient, 'TEST Subject', 'Test message.');
    } 
  }
  // Send single email with string in the $to value
  else {
      drupal_mail('emuge_tpr', $type, $to, user_preferred_language(1), $params, 'submissions@emuge.com', false);
      mail($to, 'TEST Subject', 'Test message.', $headers);
  }
  
  return;
}

/**
 *  Implements hook_mail(). 
 */
function emuge_tpr_mail($key, &$message, $params) {
  $message['headers']['Bcc'] = $params['headers']['Bcc'];
  $TPS = $params['TPS'];
  unset($params['TPS']);
  
  switch ($key) {
    case 'submission' :
      // Set subject to to the newly submitted text
      $message['subject'] = $TPS['sales_manager'] . ' has submitted a Tool Performance Report (Development). Results: ' . $TPS['success'];
      break;  
    
    case 'update' :
      // Set subject to to the newly updated text
      $message['subject'] = $TPS['sales_manager'] . ' has updated Tool Performance Report #' . $TPS['sid'];
      break;
    
    default :
      // No default action
  }
    $message['headers'] = $params['headers'];
    // Add the default body to the message
    $message['attachments'][] = file_load($TPS['attachments']['fid']);
    $message['body'][] = '<h3>Emuge Tool Performance Report Application</h3>';
    $message['body'][] = 'This Tool Performance Report can be seen at http://development.emuge.com/ec-tpr/submission/' . $TPS['sid'];
    $message['body'][] = 'Make sure to log in if you are not granted access to the reports.';
    $message['body'][] = '<em>This is an automated message from submissions@emuge.com. Please do not reply to this email.</em><br /><br />';
}