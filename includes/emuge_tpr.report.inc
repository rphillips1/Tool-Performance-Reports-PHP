<?php
/** 
 *  @file
 *    Provides the markup and display of submitted TPR reports
 *
 */

/**
 *  Function to gather and process the given $sid
 *  @param int $sid
 *    Variable that identifies the submission to display
 *
 *  @return object $page 
 *    Returns the Drupal page object to display
 */
function emuge_tpr_submission($sid) {
    
    // Check the sid received from url
    if (is_numeric($sid)) {
        
        // Load styles for this page
        drupal_add_css(drupal_get_path('module', 'emuge_tpr') . '/css/emuge_tpr_styles.css', 'file');
        
        // Iitialize table variables
        $rows = array();
        $header = array();
        $comments = array();
        
        // My method of converting machine names to human readable
        $typeInfo = array(
            'Tapping' => 'Tapping',
            'ThreadMillingFull' => 'Thread Milling Full Thread',
            'ThreadMillingSingle' => 'Thread Milling Single Plane',
            'EndMill' => 'End Milling',
            'Drilling' => 'Drilling',
            'emuge' => 'Emuge',
            'comp' => 'Competitor'
        );
        // Connect to database
        $connection = Database::getConnection('default', 'edbd');
        
        try {
            // Get the TPR records from the db
            $TPS = $connection->query('SELECT *   FROM TPS where sid = :sid', array(
                ':sid' => $sid
            ))->fetchAssoc();
        }
        catch (PDOexception $e) {
            drupal_set_message('131313 Submission View Error ' . $e->getMessage(), 'warning');
        }
        
        // Work with results if they exist. 
        if ($TPS) {
            // Save any comments, objective, and attatchments.
            if(!is_null($TPS['comments'])) {
                $comments['comments'] = $TPS['comments'];
            }
            
            if(!is_null($TPS['objective'])) {
                $comments['objective'] = $TPS['objective'];
            }
            
            if(!is_null($TPS['attachments'])) {
                $file = file_load($TPS['attachments']);
                $comments['uri'] = $file->uri;
            }
            
            // We begin to distribute neede values to the the competitor array
            $TPS['emuge_nom_dia'] = $TPS['emuge_pitch_val'];
            $TPS['comp_nom_dia'] = $TPS['comp_pitch_val'];
            
            // Call the label/value configuration function to get the 
            // appropriate fields for this report display
            $fields = _form_fields_list($TPS);
            
            // Break the field list array info sections. The sections are Tool Performance Report Data,
            // Tool Performance Report Tool Data, Test Results, and Comments
            foreach ($fields as $titles => $section) {
                $items = array();
                $cols = 0;
                $rows[] = array(
                    array(
                        'data' => '',
                        'colspan' => 1,
                        'class' => 'tps-label'
                    ),
                    array(
                        'data' => $titles,
                        'colspan' => 5,
                        'class' => 'tps-heading'
                    )
                );
                
                foreach ($section as $name => $cell) {
                    
                    $data = preg_replace('/_/', ' ', $TPS[$name]);
                    $data = preg_replace('/\(.*\]/', '', $data);
                    
                    if ($name == 'report_date')
                        $data = date('m/d/Y', $TPS[$name]);
                    
                    if ($name == 'dist' && is_numeric($TPS['dist'])) {
                        module_load_include('module', 'emuge_pc');
                        $data = _emuge_customer_load($TPS[$name]);
                        $data = $data['CustomerName'];
                    }
                    
                    if (in_array($data, array('', NULL)) && $name != 'filler_cell') {
                        $data = 'N/A';
                    }
                    
                    $colon = ($name == 'filler_cell') ? '' : ':';
                    
                    $items[] = array(
                        'data' => $cell['label'] . $colon,
                        'class' => $cell['class'] . 'tps-label '
                    );
                    
                    $items[] = array(
                        'data' => ucwords($data),
                        'colspan' => $cell['colspan'],
                        'class' => $cell['class'] . 'tps-value info-section'
                    );
                    
                    $cols += $cell['colspan'] + 1;
                    
                    if ($cols >= 6) {
                        $rows[] = $items;
                        $items = array();
                        $cols = 0;
                    } //$cols >= 6
                } //$section as $name => $cell
                
                if ($items) {
                    $items[] = array(
                        'data' => '',
                        'colspan' => 6 - ($items[(count($items) - 1)]['colspan'] - 1),
                        'class' => 'tps-label'
                    );
                    $rows[] = $items;
                    $items = array();
                } //$items
                
            } //$fields as $titles => $section
            
            // Call the function to get configuration data for 
            // Tool Performance Report Tool Data
            $testFields = _form_fields_list($TPS, 'tests');
            
            // Add the Emuge / Competitor row heading
            $rows[] = array(
                array(
                    'data' => '',
                    'colspan' => 1,
                    'class' => 'test-label'
                ),
                array(
                    'data' => 'Emuge',
                    'colspan' => 2,
                    'class' => 'tps-heading '
                ),
                array(
                    'data' => 'Competitor',
                    'colspan' => 2,
                    'class' => 'tps-heading '
                ),
                array(
                    'data' => '',
                    'colspan' => 1,
                    'class' => 'test-label'
                )
            );
            // Add the Emuge / Competitor munufacturer row as its behavior
            // is slightly different than the rest of the rows
            $rows[] = array(
                array(
                    'data' => 'Manufacturer:',
                    'colspan' => 1,
                    'class' => 'test-label '
                ),
                array(
                    'data' => 'Emuge',
                    'colspan' => 2,
                    'class' => 'emuge tps-value'
                ),
                array(
                    'data' => $testFields['Comp']['comp_manufacturer']['data'],
                    'colspan' => 2,
                    'class' => 'comp tps-value'
                ),
                array(
                    'data' => '',
                    'colspan' => 1,
                    'class' => 'test-label '
                ),
            );
            
            // Add the standard fields that apply to all TPRs
            foreach (array('init_fields', 'Parts Data', 'Time Data', 'Size Data', 'Cost Data') as $toolData) {
                
                if ($toolData != 'init_fields') {
                    $rows[] = array(
                        array(
                            'data' => '',
                            'colspan' => 1,
                            'class' => 'test-label '
                        ),
                        array(
                            'data' => $toolData,
                            'colspan' => 4,
                            'class' => 'tps-heading tps-data'
                        ),
                        array(
                            'data' => '',
                            'colspan' => 1,
                            'class' => 'test-label '
                        ),
                        
                    );
                }
                
                // This is where the algorithm gets more sopishticated. Using $i and $r 
                // we loop through all of the forms values while tracking where on the 
                // form we are
                // @TODO Better doucument this process
                foreach ($testFields[$toolData] as $key) {
                    if (startsWith($key, '_')) {
                        $emuge = array(
                            'emuge',
                            ''
                        );
                        $comp = array(
                            'comp',
                            ''
                        );
                        $i = 0;
                        $r = 0;
                    }
                    else {
                        $emuge = array('', 'test results');
                        $comp = array('', 'test results');
                        $i = 1;
                        $r = 0;
                        
                    }
                    
                    $emugeData = ucfirst($testFields[ucfirst($emuge[$i])][$emuge[$r] . $key]['data']);
                    $compData = ucfirst($testFields[ucfirst($comp[$i])][$comp[$r] . $key]['data']);
                    
                    #########################################################################################
                    # This is where I handle the report number formatting                                   #
                    #########################################################################################
                    if ($toolData == 'Parts Data' || in_array($testFields['test_labels'][$emuge[$r] . $key], array(
                        'Cutting Speed (sfm)',
                        'Spindle Speed (rpm)'
                    ))) {
                        $emugeData = number_format(preg_replace('/,/', '', $emugeData), 0);
                        $compData = number_format(preg_replace('/,/', '', $compData), 0);
                    } 
                    
                    if (in_array($testFields['test_labels'][$emuge[$r] . $key], array('Application'))) {
                        $emugeData = $typeInfo[$emugeData];
                        $compData = $typeInfo[$compData];
                    } 
                    
                    // @TODO: Find a better way to handle this. 
                    if (in_array($testFields['test_labels'][$emuge[$r] . $key], array(
                        'Feed Rate (ipm)',
                        #'Total Change Time (hr)',
                        'IPM Center Line',
                        'Depth of Cut (in)'
                    ))) {
                        $emugeData = number_format((float) $emugeData, 1);
                        $compData = number_format($compData, 1);
                    } 
                    
                    // @TODO: Find a better way to handle this. 
                    if (in_array($testFields['test_labels'][$emuge[$r] . $key], array('Total Change Time (hr)'))) {
                        $emugeData = number_format($emugeData, 2);
                        $compData = number_format($compData, 2);
                    }
                    
                    // @TODO: Find a better way to handle this. 
                    if (in_array($testFields['test_labels'][$emuge[$r] . $key], array(''))) {
                        $emugeData = number_format($emugeData, 3);
                        $compData = number_format($compData, 3);
                    }
                    
                    
                    // @TODO: Find a better way to handle this. 
                    if (in_array($testFields['test_labels'][$emuge[$r] . $key], array(
                      'Feed Rate (ipr)',
                      'Total Machine Time (hr)',
                      'Cycle Time per Hole (s)',
                      ))) {
                        $emugeData = number_format($emugeData, 4);
                        $compData = number_format($compData, 4);
                    }
                    
                    // @TODO: Find a better way to handle this. 
                    if (in_array($testFields['test_labels'][$emuge[$r] . $key], array('Drill Diameter (in)'))) {
                        $emugeData = number_format($emugeData, 4);
                        $compData = number_format($emugeData, 4);
                    }

                    // @TODO: Find a better way to handle this. 
                    if (in_array($testFields['test_labels'][$emuge[$r] . $key], array('Avg No. Holes/grind'))) {
                        $emugeData = ($TPS['emuge_regrinds_tool'] != 0) ? $TPS['emuge_holes'] / ($TPS['emuge_regrinds_tool'] + 1) : $emugeData;
                        $compData = ($TPS['comp_regrinds_tool'] != 0) ? $TPS['comp_holes'] / ($TPS['comp_regrinds_tool'] + 1) : $compData;
                    }
                    
                    ############################################################################################
                    
                    if (in_array($emugeData, array('', NULL))) $emugeData = '-';
                    if (in_array($compData, array('',NULL))) $compData = '-';
                    
                    // Using the variables, we display rows label, emuge, and comp values
                    $rows[] = array(
                        array(
                            'data' => $testFields['test_labels'][$emuge[$r] . $key] . ':',
                            'colspan' => 1,
                            'class' => 'test-label '
                        ),
                        array(
                            'data' => $emugeData,
                            'colspan' => 2,
                            'class' => 'emuge tps-value'
                        ),
                        array(
                            'data' => $compData,
                            'colspan' => 2,
                            'class' => 'comp tps-value'
                        ),
                        array(
                            'data' => '',
                            'colspan' => 1,
                            'class' => 'test-label',
                        )
                    );
                    
                }
            }
            // Add the heading row for the Test Results section
            $rows[] = array(
                array(
                    'data' => '',
                    'colspan' => 1,
                    'class' => 'test-label '
                ),
                array(
                    'data' => 'Test Results',
                    'colspan' => 2,
                    'class' => 'tps-heading results-heading'
                ),
                array(
                    'data' => '',
                    'colspan' => 2,
                    'class' => 'tps-heading results-heading'
                ),
                array(
                    'data' => '',
                    'colspan' => 1,
                    'class' => 'test-label',
                )
            );
            
            // Set the class of the results field to display dynamic styles based on success/fail
            $ResultsClass = strtolower($testFields['Test results']['success']['data']);
            
            // Add a negative sign in front of dollar amounts that are 'fail'
            if ($ResultsClass == 'fail') {
                $testFields['Test results']['job_savings'] = preg_replace('/\$-/', '-$', $testFields['Test results']['job_savings']);
                $testFields['Test results']['part_savings'] = preg_replace('/\$-/', '-$', $testFields['Test results']['part_savings']);
            } //$ResultsClass == 'fail'
            
            // Determine job, tool, and percent savings for report
            foreach ($testFields['Test Results'] as $key) {
                if ($key == 'job_savings_percent') {
                    $eCost = preg_replace('/\$|\,/', '', $TPS['emuge_total_cost']);
                    $cCost = preg_replace('/\$|\,/', '', $TPS['comp_total_cost']);
                    $percent = number_format((1 - ($eCost / $cCost)) * 100, 1);
                    $testFields['Test results'][$key]['data'] = $percent . '%';
                }
                
                // Append the above results to report table
                $rows[] = array(
                    array(
                        'data' => $testFields['test_labels'][$key],
                        'colspan' => 1,
                        'class' => 'test-label tps-label ' . $ResultsClass
                    ),
                    array(
                        'data' => $testFields['Test results'][$key]['data'],
                        'colspan' => 2,
                        'class' => 'total results tps-value ' . $ResultsClass
                    ),
                    array(
                        'data' => '',
                        'colspan' => 2,
                        'class' => 'total results tps-value ' . $ResultsClass
                    ),
                    array(
                        'data' => '',
                        'colspan' => 1,
                        'class' => 'tps-label'
                    )
                );
            }
           
            // The comments area does not have a specific section. We append the tables if they exits
            // Add objective is it exists
            if(isset($comments['objective'])) {
                $rows[] = array(
                    array(
                        'data' => 'Objective:',
                        'class' => ' test-label ',
                    ),
                    array(
                        'data' => $comments['objective'],
                        'colspan' => 4,
                        'class' => 'comments', 
                    ),
                    array(
                        'data' => '',
                        'colspan' => 1,
                        'class' => 'test-label',
                    ),
                );
            }
            // Add comments if they exists
            if(isset($comments['comments'])) {
                $rows[] = array(
                    array(
                        'data' => 'Comments:',
                        'class' => 'test-label',
                    ),
                    array(
                        'data' => $comments['comments'],
                        'colspan' => 4,
                        'class' => 'comments',
                    ),
                    array(
                        'data' => '',
                        'colspan' => 1,
                        'class' => 'test-label',
                    ),
                );
            }
            // Add a link to the user uploaded file, if applicable.     
            if(isset($comments['uri'])) {
                 $rows[] = array(
                    array(
                        'data' => '',
                        'class' => ' tps-label attachment',
                    ),
                    array(
                        'data' => '<a href="' . file_create_url($comments['uri']) . '" target="new">Attachment</a>',
                        'colspan' => 4,
                        'class' => 'tps-label',
                    ),
                    array(
                        'data' => '',
                        'colspan' => 1,
                        'class' => 'test-label',
                    ),
                );
            }
        }
        // If the provided TPR does not return a valid TPS, provide a message
        else {
            drupal_set_message('Report #' . $sid, 'error');
            $rows[] = array(
                'No - Report #' . $sid . ' does not exists.'
            );
        }
    }
    // @GIGO: Redirect back to dashboard with incorrect/injection URI 
    else {
        // Drupal_set_message('Please try your selection again. Contact web.developer@emuge.com if this problem persists.', 'warning');
        drupal_set_message('Report #' . $sid . ' is NaN', 'error');
        drupal_goto($base_root . 'ec-tpr');
    }
    
    // Get the url path
    $path = $_GET['q'];
    $links = 
  '<div class="ec-tpr-links hide-links">
     <a href="/ec-tpr/search/all" style="float:right;" title="View All Emuge Tool Performance Reports" class="boxed-button ec-tpr-link">all</a>
     
     <a href="/ec-tpr/search/my" style="float:right;" title="View My Emuge Tool Performance Reports" class="boxed-button ec-tpr-link">my</a>
     
     <a href="/printpdf/' . $path . '" style="float:right;" title="Save Emuge Tool Performance Report #' . $sid . '" class="boxed-button ec-tpr-link">pdf</a>
     
     <a href="/emugemail/' . $path . '" style="float:right;" title="Email Emuge Tool Performance Report #' . $sid . '" class="boxed-button ec-tpr-link">email</a>
     
     <a href="/print/' . $path . '" style="float:right;" title="Print Emuge Tool Performance Report #' . $sid . '" class="boxed-button ec-tpr-link">print</a>
          </div>';
  
    // Repeat the above links, changing the class for styling purposes
    $bottomLinks = preg_replace('/ec-tpr-links/', 'ec-tpr-links-bottom', $links);
    if(module_exists('emugemail')) {
        $emugemail_path = '/emugemail/' . $_GET['q'];
        $emugemail_link = '<div class="ec-tpr-links hide-links"><a style="margin-left: 125px; float: left;
        margin-top: 8px;" class="print-mail" href="' . $emugemail_path . '">Email</a></div>';
    }
    else {
        $emugemail_link = '';   
    }
    // Return     
    return $page['#content'] .= $links . theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
            'width' => '100%'
        ),
    ));
}

/**
 *  Function to keep track and provide the needed fields for 
 *  the TPR submission display
 *
 *  @param object $TPS
 *    Custom submission object reference varialble
 *
 *  @param string $layout
 *    String to determine the layout to use. Used for potential 
 *    future layouts.
 * 
 *  @param array $tests
 *
 *  @return array $fields 
 *    Returns an associative array of field names    
 */
function _form_fields_list(&$TPS, $layout = 'page', $tests = array()) {
    switch ($layout) {
        case 'page':
            $fields = array();
            
            $fields['Tool Performance Report Data'] = array(
                'report_date' => array(
                    'label' => 'Report Date',
                    'colspan' => 1,
                    'class' => ''
                ),
                'sales_area' => array(
                    'label' => 'Country',
                    'colspan' => 1,
                    'class' => ''
                ),
                'sales_manager' => array(
                    'label' => 'Sales Manager',
                    'colspan' => 1,
                    'class' => ''
                ),
                'dist' => array(
                    'label' => 'Distributor',
                    'colspan' => 1,
                    'class' => ''
                ),
                'dist_contact' => array(
                    'label' => 'Contact',
                    'colspan' => 1,
                    'class' => ''
                ),
                'filler_cell' => array(
                    'label' => '',
                    'colspan' => 1,
                    'class' => 'tps-spacer '
                ),
                'end_user' => array(
                    'label' => 'End User',
                    'colspan' => 1,
                    'class' => ' hide-links '
                ),
                'end_user_contact' => array(
                    'label' => 'Contact',
                    'colspan' => 1,
                    'class' => ' hide-links '
                )
            );
            
            // updating per Bobs request
            $fields['Tool Performance Report Tool Data'] = array(
                'emuge_size' => array(
                    'label' => 'Size',
                    'colspan' => 1
                ),
                'tool_type' => array(
                    'label' => 'Machine Type',
                    'colspan' => 1
                ),
                'class_fit' => array(
                    'label' => 'Class of Fit',
                    'colspan' => 1
                ),
                'tool_brand' => array(
                    'label' => 'Machine Brand',
                    'colspan' => 1
                ),
                'machining_plane' => array(
                    'label' => 'Machining Plane',
                    'colspan' => 1
                ),
                'machine_tool_condition' => array(
                    'label' => 'Machine Condition',
                    'colspan' => 1
                ),
                'workpiece_desc' => array(
                    'label' => 'Workpiece Type',
                    'colspan' => 1
                ),
                'workpiece_hardness' => array(
                    'label' => 'Hardness',
                    'colspan' => 1
                ),
                'material' => array(
                    'label' => 'Material',
                    'colspan' => 1
                ),
                'holder_condition' => array(
                    'label' => 'Holder Condition',
                    'colspan' => 1
                ),
                'coolant_lube' => array(
                    'label' => 'Coolant/Lube',
                    'colspan' => 1
                ),
                'submaterial' => array(
                    'label' => 'Sub-Material',
                    'colspan' => 1
                ),
                'holder_type' => array(
                    'label' => 'Holder Type',
                    'colspan' => 1
                ),
                'coolant_psi' => array(
                    'label' => 'Coolant PSI',
                    'colspan' => 1
                ),
                'iso_material_group' => array(
                    'label' => 'ISO Group',
                    'colspan' => 1
                )
            );
            
            return $fields;
            break;
        
        case 'tests':
            foreach ($TPS as $key => $value) {
                if (in_array($TPS[$key], array('',NULL)) || !isset($TPS[$key])) {
                    $TPS[$key] = '-';
                }
                if (startsWith($key, 'emuge')) {
                    if ($TPS['emuge_application'] != 'drilling' && $key == 'emuge_regrind_total_cost') {
                    } 
                    elseif (endsWith($key, 'cost') || endsWith($key, 'price')) {
                        $TPS[$key] = '$' . number_format((float) $value, 2);
                        
                        if (endsWith($key, 'total_cost')) {
                            $class = 'total ';
                        } 
                        else {
                            $class = '';
                        }
                        
                        $fields['Emuge'][$key] = array(
                            'data' => $TPS[$key],
                            'colspan' => 2,
                            'label' => $key,
                            'class' => $class . 'emuge-costs '
                        );
                    }
                    else {
                        $fields['Emuge'][$key] = array(
                            'data' => $value,
                            'colspan' => 2,
                            'label' => $key,
                            'class' => 'emuge '
                        );
                    }
                }
                else if (startsWith($key, 'comp')) {
                    
                    if ($TPS['comp_application'] != 'drilling' && $key == 'comp_regrind_total_cost') {
                    }
                    elseif (endsWith($key, 'cost') || endsWith($key, 'price')) {
                        if (endsWith($key, 'total_cost')) {
                            $class = 'total ';
                        } 
                        else {
                            $class = '';
                        }
                        $TPS[$key] = '$' . number_format($value, 2);
                        $fields['Comp'][$key] = array(
                            'data' => $TPS[$key],
                            'colspan' => 2,
                            'label' => $key,
                            'class' => $class . 'comp-costs '
                        );
                    }
                    else {
                        $fields['Comp'][$key] = array(
                            'data' => $value,
                            'colspan' => 2,
                            'label' => $key,
                            'class' => 'comp '
                        );
                    }
                }
                else if (in_array($key, array(
                        'success',
                        'job_savings',
                        'part_savings',
                        'hourly_rate'
                    ))) {
                    if (endsWith($key, 'savings') || endsWith($key, 'rate')) {
                        if ($key == 'job_savings') {
                            
                            $color = ($TPS[$key] > 0) ? 'success ' : 'fail ';
                        } 
                        else {
                            
                            $color = '';
                        }
                        
                        $TPS[$key] = '$' . number_format($TPS[$key], 2);
                        
                        $fields['Test results'][$key] = array(
                            'data' => $TPS[$key],
                            'colspan' => 5,
                            'label' => $key,
                            'class' => $color . 'results '
                        );
                    }
                    else {
                        
                        $fields['Test results'][$key] = array(
                            'data' => $TPS[$key],
                            'colspan' => 5,
                            'label' => $key,
                            'class' => 'test results '
                        );
                    }
                } 
                else {
                    
                    $fields['Test results'][$key] = array(
                        'data' => $TPS[$key],
                        'colspan' => 2,
                        'label' => $key,
                        'class' => $color . 'results '
                    );
                }
            }
            
            $connection = Database::getConnection('default', 'edbd');
            
            try {
                $labels = $connection->query('SELECT * FROM TPS WHERE sid = 1919')->fetchAssoc();
            }
            catch (PDOexception $e) {
                drupal_set_message($e->getMessage(), 'error');
            }
            
            if($TPS['emuge_application'] == 'drilling') {
                $labels['original_holes'] = 'Avg No. Holes/grind';
            }
            
            $fields['test_labels'] = $labels;
            
            foreach ($fields['test_labels'] as $field => $label) {
                if (endsWith($label, '(in)')) {
                    if (startsWith($field, 'emuge')) {
                        $co = 'Emuge';
                    } 
                    elseif (startsWith($field, 'comp')) {
                        $co = 'Comp';
                    }
                    else {
                        $co = '';
                    }
                    $fields[$co][$field]['data'] = number_format($fields[$co][$field]['data'], 4);
                } 
            }
            
            $IPR = ($TPS['emuge_application'] == 'drilling') ? '_IPR' : '_pitch_val';
            
            $fields['init_fields'] = array(
                '_application',
                '_part_num',
                '_part_price'
            );
            
            if($TPS['emuge_application'] != 'drilling') {
                $partsData = array('holes_part', 'parts_job', 'holes_job', '_holes', '_tools_job');
            }
            else if($TPS['emuge_application'] == 'drilling' && ($TPS['emuge_regrinds_tool'] != 0 || $TPS['comp_regrinds_tool'] != 0)) {
                $partsData = array('holes_part', 'parts_job', 'holes_job', 'original_holes','_holes', '_tools_job');
            } 
            else if($TPS['emuge_application'] == 'drilling') {
                $partsData = array('holes_part', 'parts_job', 'holes_job','_holes', '_tools_job');
            }
            
            $fields['Parts Data'] = $partsData ;
            
            $fields['Time Data'] = array(
                '_cutting_speed',
                '_spindle',
                '_cycle_time_ph',
                'change_time',
                '_total_change_time',
                '_mach_time_hr'
            );
            $fields['Size Data'] = array(
                '_IPM',
                $IPR,
                '_thread_depth',
                '_drill_size_in'
            );
            $fields['Cost Data'] = array(
                'hourly_rate',
                '_tool_cost',
                '_mach_cost',
                '_change_cost'
            );
            
            if ($TPS['emuge_application'] != 'drilling') {
                array_push($fields['Size Data'], '_nom_dia', '_pitch_val');
                
                if (!in_array($TPS['comp_application'], array(
                    'Tapping',
                    'tapping',
                    '-',
                    '',
                    NULL
                )) || !in_array($TPS['emuge_application'], array(
                    'Tapping',
                    'tapping'
                ))) {
                    array_push($fields['Parts Data'], '_thread_number', '_flutes', '_passes');
                    array_push($fields['Size Data'], '_ipm_center');
                } 
            }
            else if ($TPS['emuge_regrinds_tool'] != 0 || $TPS['comp_regrinds_tool'] != 0) {
                array_pop($fields['Parts Data']);
                array_push($fields['Parts Data'], '_regrinds_tool', '_tools_job', '_regrinds_job');
                array_push($fields['Cost Data'], '_regrind_tool_cost', '_regrind_total_cost');
            } 
            else {
                array_pop($fields['Parts Data']);
                array_push($fields['Parts Data'], '_tools_job');
            }
            
            array_push($fields['Cost Data'], '_total_cost');
            
            $fields['Test Results'] = array(
                'job_savings',
                'part_savings',
                'job_savings_percent',
                'success'
            );
            break;
        
        default:
            // There is no default value for this switch
    } //$layout
     variable_del('emugetpr_fields');
    return $fields;
}


function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}