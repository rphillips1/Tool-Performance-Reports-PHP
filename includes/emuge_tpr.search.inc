<?php

/**
 * return a form for searching Tool Performance Submissions
 */ 
function emuge_tpr_search_form($form, &$form_state) {
      
    $connection = Database::getConnection('default', 'edbd');
    
    $query = db_select('emugecom_territory_managers', 'm')
        ->fields('m', array('first', 'last', 'uid'))
        ->condition('uid', 833, '!=')
        ->execute();
    
    $managers = $query->fetchAll();
    
    $query = $connection->select('mat_submat', 's')
        ->fields('s', array('submaterial'))
        ->execute();
    
    $submaterials = $query->fetchAll();
    
    $options = array(
        'managers' => array(
            'All' => 'All',
        ),
        'submaterials' => array(
            'All' => 'All',
        ),
    );
    
    foreach($managers as $manager) {
        $options['managers'][$manager->uid] = $manager->first . ' ' . $manager->last;   
    };
    
    foreach($submaterials as $submaterial) {
        $options['submaterials'][$submaterial->submaterial] = html_entity_decode(preg_replace('/_/', ' ', $submaterial->submaterial));
    };
    
    $form['report_num'] = array(
        '#type' => 'textfield',
        '#title' => t('Report Number'),      
    );
  
    $form['manager'] = array(
        '#type' => 'select',
        '#title' => t('Manager'), 
        '#options' => $options['managers'],
        '#default_value' => '',     
    );
  
    $form['workpiece'] = array(
        '#type' => 'fieldset',
        '#title' => 'Materials',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
  
    $form['workpiece']['material'] = array(
        '#type' => 'select',
        '#title' => t('Material'),
        '#options' => drupal_map_assoc(array(
            t('All'),t('Steel'), t('Stainless Steel'), t('Cast Iron'), t('Aluminum Alloys'),
            t('Copper Alloys'), t('Composites'), t('Magnesium Alloys'), t('Titanium Alloys'), 
            t('Nickel Alloys'))
        ),
        '#default_value' => 'All',
    );
  
    $form['workpiece']['submaterial'] = array(
        '#type' => 'select',
        '#title' => t('Sub-Material'),
        # set up to use existing ajax fucntions
        '#options' => $options['submaterials'],
        '#default_value' => '',
    );
  
    $form['date_range'] = array(
        '#type' => 'fieldset',
        '#title' => 'Date',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
  
    $today = date("Y-m-d H:i:s");
    $start = date("2014-12-1 00:00:00");

    $form['date_range']['from_date'] = array(
        '#type' => 'date_popup', // types 'date_text' and 'date_timezone' are also supported. See .inc file.
        '#title' => t('From Date'),
        '#date_format' => 'm/d/Y',
        '#default_value' => $start,
        '#attributes' => array(
            'class' => array('datepicker'),
        ),
    );
   
    $form['date_range']['to_date'] = array(
        '#type' => 'date_popup', // types 'date_text' and 'date_timezone' are also supported. See .inc file.
        '#title' => t('To Date'),
        '#date_format' => 'm/d/Y',
        '#default_value' => $today,
        '#attributes' => array(
            'class' => array('datepicker'),
        ),
    );

    $form['tests'] = array(
        '#type' => 'fieldset',
        '#title' => 'Test Types',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
   
    $form['tests']['test_types'] = array(
        '#title' => 'Test Type(s) (Leave unchecked for all types)',
        '#type' => 'checkboxes',
        '#options' => array(
            'tapping' => t('Tapping'),
            'threadMillingSingle' => t('TM - Single Plane'),
            'threadMillingFull' => t('TM - Full Thread'), 
            'drilling' => t('Drilling'), 
            'endMilling' => t('End Milling'),
        ),
    );
    
        $form['tests']['test_result'] = array(
        '#title' => 'Test Result',
        '#type' => 'checkboxes',
        '#options' => drupal_map_assoc(array(t('Success'), t('Fail'))),
        '#default_value' => array('Success', 'Fail'),  
    );
    
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Search TP Reports'),
        '#submit' => array('_emuge_tpr_search_form_results'),
        '#attributes' => array(
            'class' => array('boxed-button')
        ),
        '#ajax' => array(
            'callback' => '_emuge_tpr_search_form_results',
            'wrapper' => 'search-results-div',
            'method' => 'replace',
            'effect' => 'none',
        ),
    );
  
    $form['clear'] = array(
        '#type' => 'submit',
        '#attributes' => array(
            'class' => array('boxed-button')
        ),
        '#value' => t('Reset Form'),
    );
    
    $form['results_fieldset'] = array(
        '#title' => 'Markup',
        '#type' => 'markup',
        '#prefix' => '<div id="search-results-div">',
        '#suffix' => '</div>',
    );
   
   return $form;
}

/*
 * return a formatted table of TPSs 
 */
function _emuge_tpr_search_form_results($form, &$form_state) {

  $header = array(
        0 => array(
            'data' =>'Report'),
        1 => array(
            'data' =>'Material'), 
        2 => array(
            'data' =>'Sub-Material'),
        3 => array(
            'data' =>'Emuge Type'), 
        4 => array(
            'data' =>'EDP#'), 
        5 => array(
            'data' =>'Result'), 
        6 => array(
            'data' =>'Manager'), 
        7 => array(
            'data' =>'Date'),
        );
  
  if($_POST['report_num'] != '') {
    $conditions['sid'] = $_POST['report_num'];
  }
  // For the rest of the conditions, we do not evalutate
  // if there exists an sid, as this implies one specific result
  if($_POST['manager'] != 'All' && !$conditions['sid']) {
    $conditions['uid'] = $_POST['manager'];
  }
  if($_POST['material'] != 'All'  && !$conditions['sid']) {
    $conditions['material'] = preg_replace('/ /', '_', $_POST['material']);
  }
  if($_POST['submaterial'] != 'All'  && !$conditions['sid']) {
    $conditions['submaterial'] = preg_replace('/ /', '_', $_POST['submaterial']);
  }
  if(count($_POST['test_result']) == 1  && !$conditions['sid']) {
    $conditions['success'] = $_POST['test_result'];
  } 
  if($_POST['from_date'] != '' && !$conditions['sid']) {
    $conditions['frDate'] = strtotime($_POST['from_date']['date']);
  }
  if($_POST['to_date'] != '' && !$conditions['sid']) {
    $conditions['toDate'] = strtotime($_POST['to_date']['date']);
  }
  if(count((array)$_POST['test_types']) != 0 && !$conditions['sid']) {
    foreach($_POST['test_types'] as $type) {
      $conditions['emuge_application'][] = $type;
    }
  }
  
  $TPS = Database::getConnection('default', 'edbd')
      ->select('TPS', 't')
      ->fields('t', array('uid', 'sid', 'material', 'submaterial', 'success', 'sales_manager', 'emuge_application', 'emuge_part_num', 'nid', 'report_date', 'is_draft'));
   
  foreach($conditions as $key => $val) {
    if($key == 'sid') {
      $TPS->condition('sid', $val);
        break;
    }
    
    $field = $key;
    $sign = '=';
    
    if($key == 'frDate') {$sign = '>='; $field = 'report_date';}
    if($key == 'toDate') {$sign = '<='; $field = 'report_date';}   
    if($key == 'emuge_application') {$sign = 'IN';}
      
    $TPS->condition($field, $val, $sign);
   
  }
  
  $TPS->orderBy('sid', 'DESC');
   
  try{
    $TPS = $TPS->execute()
      ->fetchall(); 
  } 
  catch (PDOexception $e) {
    $markup .= $e->getMessage();
  }
  
  foreach($TPS as $record) {
    if($record->is_draft == 0) {
        
      if(in_array($record->emuge_application, array('threadMillingSingle', 'threadMillingFull'))) {
          $record->emuge_application = ($record->emuge_application == 'threadMillingSingle') ?
            'Milling Single' : 'Milling Full';
      }
      
      $rows[] = array(
        0 => array(
            'data' => '<a style="text-decoration:underline;" href="/ec-tpr/submission/' . $record->sid . '" title="View Report #' . $record->sid . '" onclick="window.open(this.href, \'\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=1000,height=950\'); return false;">' .
              $record->sid . '</a>'),
        1 => array(
            'data' => substr(preg_replace('/_/', ' ', $record->material), 0, 16)),
        2 => array(
            'data' => substr(preg_replace('/_/', ' ', $record->submaterial), 0, 16)), 
        3 => array(
            'data' => substr(ucfirst($record->emuge_application), 0, 16)), 
        4 => array(
            'data' => substr(strtoupper($record->emuge_part_num), 0, 16)), 
        5 =>  array(
            'data' => $record->success),
        6 => array (
            'data' => $record->sales_manager), 
        7 => array(
            'data' => date('m/d/Y', $record->report_date)),
       );   
    }
  } 
      
  $markup .= '<div id="search-results-div">';
  
  if($rows) {
    $markup .=  'Tool Performance Submissions Search Results';
    $markup .= theme('table', array('header' => $header, 'rows' => $rows));   
  } 
  else {
    $markup .=  'No Results fit your criteria';
  }
     
  $markup .= '</div>';
  
  return $form['results_fieldset']['#markup'] = $markup;
}